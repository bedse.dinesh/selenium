import unittest
from UnitTest_FW.Package1.TC_LoginTest import LoginTest
from UnitTest_FW.Package1.TC_Signup_Test import SignupTest

from UnitTest_FW.Package2.TC_PaymentTest import PaymentTest
from UnitTest_FW.Package2.TC_PaymentReturnsTest import PaymentReturnsTest

# Get all test from LoginTest. SignUpTest, PaymentTest, PaymentReturnsTest

tc1 = unittest.TestLoader().loadTestsFromTestCase(LoginTest)
tc2 = unittest.TestLoader().loadTestsFromTestCase(SignupTest)
tc3 = unittest.TestLoader().loadTestsFromTestCase(PaymentTest)
tc4 = unittest.TestLoader().loadTestsFromTestCase(PaymentReturnsTest)

#Creating test suites
sanityTestSuite = unittest.TestSuite([tc1, tc2])  # Sanity Test Suites
functionalTestSuite = unittest.TestSuite([tc3, tc4])  # Functional Test Suites
masterTestSuite = unittest.TestSuite([tc1, tc2, tc3, tc4])  # Master Test Suites

unittest.TextTestRunner().run(sanityTestSuite)
unittest.TextTestRunner().run(functionalTestSuite)
unittest.TextTestRunner(verbosity=2).run(masterTestSuite)  # verbosity -- Detailed logs in console window
