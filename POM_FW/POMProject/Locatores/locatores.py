class Locators:
    ###TestData###
    driver_path = 'D:/Dinesh/Projects/Selenium/Drivers/chromedriver.exe'
    url = 'https://opensource-demo.orangehrmlive.com/'
    username = 'Admin'
    password = 'admin123'

    ##login page objects
    username_textbox_id = 'txtUsername'
    password_textbox_id = 'txtPassword'
    login_btn_id = 'btnLogin'

    ##home page objects
    welcome_link_id = 'welcome'
    logout_link_linkText = 'Logout'


###Login###
# tab_signup = '//a[contains(text(),"Login & Signup")]'
# close_button = '//button[@class="_2AkmmA _29YdH8"]'
# button_request_otp = '//button[text()="Request OTP"]'
# input_email_text = '//input[@type="text" and @class="_2zrpKA _1dBPDZ"]'
# input_password_text = '//input[@type="password" and @class="_2zrpKA _3v41xv _1dBPDZ"]'
# button_login = '//button/span[text()="Login"]'
