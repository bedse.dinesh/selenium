import os
import threading
import paramiko
import subprocess
import sys
import keyboard

ip = '10.100.109.81'
user = 'pcg'
passwd = 'pcgtest'

tom = '--image tom-15.5.1.64-3846532-tcf5fpga-class0+apollo-2-00'
lascar = '--image lascar-22.104.208.318-5124208-tcfvudual-class0+odin-3.10'

def connect():
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(ip, username=user, password=passwd)

    print("SSH connection established to %s" % ip)
    remote_conn = client.invoke_shell()
    print("Interactive SSH session established")
    output = remote_conn.recv(1000)
    print(output)

try:
    connect()
except paramiko.ssh_exception.SSHException:
    keyboard.read_key() == 'r'
    print('SSHException Exception')
    connect()
except paramiko.ssh_exception.NoValidConnectionsError:
    keyboard.read_key() == 'r'
    print('NoValidConnectionsError Exception')
    connect()

