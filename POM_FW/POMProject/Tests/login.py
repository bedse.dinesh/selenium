from selenium import webdriver
import time

from POM_FW.POMProject.Locatores.locatores import Locators
import unittest
from POM_FW.POMProject.Pages.loginPage import *
from POM_FW.POMProject.Pages.homePage import *


class LoginTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Chrome(Locators.driver_path)
        cls.driver.implicitly_wait(5)
        cls.driver.maximize_window()

    def test_login_valid(self):
        driver = self.driver
        self.driver.get(Locators.url)
        login = LoginPage(driver)
        login.enter_username(Locators.username)
        login.enter_password(Locators.password)
        login.click_login()

        homepage = HomePage(driver)
        #homepage.click_on_welcome()
        #homepage.click_on_logout()
        time.sleep(10)

    @classmethod
    def tearDownClass(cls):
        #cls.driver.close()
        #cls.driver.quit()
        print('Test Completed')


if __name__ == '__main__':
    unittest.main()
