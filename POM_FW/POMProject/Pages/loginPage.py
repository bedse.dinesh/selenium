from POM_FW.POMProject.Locatores.locatores import *


class LoginPage():
    def __init__(self, driver):
        self.driver = driver
        self.username_textbox_id = Locators.username_textbox_id
        self.password_textbox_id = Locators.password_textbox_id
        self.login_btn_id = Locators.login_btn_id

    def enter_username(self, user_name):
        self.driver.find_element_by_id(self.username_textbox_id).clear()
        self.driver.find_element_by_id(self.username_textbox_id).send_keys(user_name)

    def enter_password(self, pass_word):
        self.driver.find_element_by_id(self.password_textbox_id).clear()
        self.driver.find_element_by_id(self.password_textbox_id).send_keys(pass_word)

    def click_login(self):
        self.driver.find_element_by_id(self.login_btn_id).click()
