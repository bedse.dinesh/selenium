import pytest


@pytest.yield_fixture()
def setup():
    print('Once Before Every Method')
    yield
    print('Once After Every Method')


def testMethod1(setup):
    print('This is Test Method1')


def testMethod2(setup):
    print('This is Test Method2')
